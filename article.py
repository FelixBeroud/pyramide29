class Article:
    """Classe implémentant les articles Wikipedia.

    Attributes
    ----------
    titre : str
        titre de l'article Wikipedia
    texte : str
        texte contenu dans l'article
    titre_court : str
        titre de l'article auquel on a retiré les précisions entre parenthèses
    """

    def __init__(self, titre, texte=""):
        """Initialisation de la classe"""
        self.titre = titre
        self.texte = texte
        self.titre_court = self.get_titre_court()
    
    def get_titre_court(self):
        """Méthode renvoyant le titre court de l'article, donc le titre sans la précision éventuelle entre parenthèses"""
        if "(" in self.titre and ")" in self.titre:
            parenthese1 = self.titre.index("(")
            return self.titre[:parenthese1-1]
        else:
            return self.titre
