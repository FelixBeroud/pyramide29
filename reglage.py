class Reglage:
    """Classe permettant de construire des niveaux de difficulté en jouant sur les paramètres de la partie avec le nombre d'indices proposés, le nombre de tentatives possibles, la catégorie choisie et un facteur multiplicatif.

    Arguments
    ---------
    nombre_indices_init (int):
        nombre d'indices de départ dans une partie
    nombre_tentatives_max (int):
        nombre de tentatives au total disponible pour l'utilisateur
    facteur_multip (float):
        modificateur de score de la partie
    difficulte (str):
        difficulté (Facile, Moyen, Difficile)
    categorie (str):
        mode de jeu (Tout Wikipedia, ou Catégories : Animaux, Pays, Villes, Personnes célèbres, Divers ou Aléatoire)
    """

    def __init__(self, nombre_indices_init = 3, nombre_tentatives_max = 5, facteur_multip = 1.5, difficulte="Moyen", categorie="Tout Wikipédia"):
        """Initialisation de la classe"""
        
        self.nombre_indices_init = nombre_indices_init
        self.nombre_tentatives_max = nombre_tentatives_max
        self.facteur_multip = facteur_multip
        self.difficulte = difficulte
        self.categorie = categorie
