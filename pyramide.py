from InquirerPy import inquirer
from InquirerPy.base.control import Choice
from InquirerPy.validator import PasswordValidator
from dao.joueusedao import JoueuseDAO
from reglage import Reglage
from partie import Partie
from login import Login
from joueuse import Joueuse
from dao.requetedao import RequeteDAO
from dao.interfacedao import InterfaceDAO

class Pyramide:
    """Classe regroupant toutes les fonctionnalités générales du jeu et introduisant la controller layer du jeu.
    
    Elle ne possède pas d'attributs."""
    
    def se_connecter(self):
        """Méthode permettant à l'utilisateur de se connecter (s'il possède déjà un compte)
        
        Returns
        -------
        list : le premier élément est le statut de l'authentification (False si échouée, True sinon), le deuxième est le pseudo du joueur, réutilisé plus tard.
        """

        pseudo = inquirer.text(message = 'Pseudo : ').execute() 
        password = inquirer.secret(message='Mot de passe : ', transformer=lambda _: "").execute() #permet de ne remplacer le mot de passe écrit par des astériques (*) et une fois que le mot de passe est entré, le mot de passe est effacé avec l'option du transformer
        
        login_test = Login(pseudo, password) 
        login_test.chiffrer_mdp() # Seul le mot de passe chiffré est enregistré

        rep = RequeteDAO().checker_mdp(login_test) # la DAO va rechercher si la correspondance entre le pseudo et le mot de passe est correcte

        if rep == 1:
            return [True, pseudo]
        else:
            print("Mauvais pseudo ou mauvais mot de passe")
            return [False, ""]

    def creer_un_compte(self):
        """Méthode permettant à l'utilisateur de se créer un compte
        
        Returns
        -------
        str : le nouveau pseudo de l'utilisateur
        """

        pseudo = inquirer.text(message = 'Choisis un pseudo : ').execute()

        password = inquirer.secret(message='Choisis un mot de passe : ',
        validate=PasswordValidator(length=6, number=True),
        transformer=lambda _: "",
        long_instruction="Le mot de passe doit contenir au moins 6 caractères dont un chiffre.").execute()
        # les exigences sont de 6 caractères minimum, au moins un chiffre. Si elles ne sont pas respectées, l'utilisateur doit rentrer à nouveau un mot de passe, jusqu'à ce qu'il convienne aux critères de sécurité.

        login_local = Login(pseudo,password)
        login_local.chiffrer_mdp() # Seul le mot de passe chiffré est enregistré

        nouveau_compte = Joueuse(login_local)
        JoueuseDAO().creer_joueuse(nouveau_compte) # Ajoute le joueur dans la base de données via la DAO

        return pseudo


    def jouer(self, pseudo):

        #Options parmi lesquelles le joueur peut choisir. Il s'agit d'un réglage.
        categorie = inquirer.select(
            message="Choisis une catégorie ou joue dans tout Wikipedia :",
            choices=[
                "Tout Wikipédia",
                "Villes",
                "Pays",
                "Animaux",
                "Personnes célèbres",
                "Divers",
                "Aléatoire"
            ],
            long_instruction ="L'option 'Tout Wikipédia' peut occasionner un temps de chargement long."
        ).execute()

        #Difficultés parmi lesquelles le joueur peut choisir. Il s'agit également d'un réglage.
        difficulte = inquirer.select(
            message="Choisis un niveau de difficulté :",
            choices=[
                "Facile",
                "Moyen",
                "Difficile",
                "Jouer sur un set de test"
            ],
            long_instruction ="\nFacile : 5 indices au départ, 7 tentatives\nMoyen : 3 indices au départ, 5 tentatives \nDifficile : 3 indices au départ, 3 tentatives.",
            default=None,
        ).execute()

        # Création de l'objet Réglage en fonction des choix précédents.
        if difficulte == "Facile":
            reglages = Reglage(nombre_indices_init = 5, nombre_tentatives_max = 7, facteur_multip = 1, difficulte="Facile")
        elif difficulte == "Moyen":
            reglages = Reglage(nombre_indices_init = 3, nombre_tentatives_max = 5, facteur_multip = 1.5, difficulte="Moyen")
        elif difficulte == "Jouer sur un set de test":
            reglages = Reglage(nombre_indices_init = 5, nombre_tentatives_max = 7, facteur_multip = 1, difficulte="Jouer sur un set de test")
        else:
            reglages = Reglage(nombre_indices_init = 3, nombre_tentatives_max = 3, facteur_multip = 2, difficulte="Difficile")
        
        reglages.categorie = categorie


        #Initialisation et lancement de la nouvelle partie
        nouvelle_partie = Partie(score_partie = 0, joueuse=Joueuse(login=Login(nom=pseudo)), reglage=reglages)
        sauvegarde = nouvelle_partie.start_partie()

        #Sauvegarde des scores si le joueur en a décidé ainsi 
        if sauvegarde == "Oui !":
            InterfaceDAO().updater_bdd(nouvelle_partie)
        else:
            print("") #Saut de ligne simplement pour la lisibilité

    def main(self):

        # Ecran "d'accueil" du jeu : le joueur doit choisir de se connecter ou de créer un compte pour pouvoir continuer.
        action1 = inquirer.select(
            message="Choisis une action :",
            choices=[
                "Se connecter",
                "Créer un compte",
                Choice(value=None, name="Quitter"),
            ],
            default=None,
        ).execute()

        if action1 == "Se connecter":
            authentification = False
            while authentification == False: # le joueur doit réessayer tant qu'il n'a pas retrouvé son mot de passe
                [authentification, pseudo] = self.se_connecter()
        if action1 == "Créer un compte":
            pseudo = self.creer_un_compte()
        
        # Après la connexion ou la création de compte, le joueur peut jouer ou consulter la base de données.
        action2 = inquirer.select(
            message="Choisis une action :",
            choices=[
                "Jouer",
                "Consulter la BDD",
                Choice(value=None, name="Quitter"),
            ],
            default=None,
        ).execute()

        partie_en_cours = True #booléen qui permet d'arrêter la boucle des parties si le joueur veut arrêter de jouer.

        while partie_en_cours:
            if action2 == "Jouer":
                self.jouer(pseudo) # Lance une partie
                
                action3 = inquirer.select(
                    message="Souhaites-tu refaire une partie ?",
                    choices=[
                        "Oui !",
                        "Non",
                    ],
                    default=None,
                ).execute()

                if action3 == "Non":
                    partie_en_cours = False # Fin de la boucle dans ce cas. Sinon, une autre partie reprend (nouveau tour de boucle)

            elif action2 == "Consulter la BDD":
                actionbdd = inquirer.select(
                    message = "\nQue voulez-vous faire ?",
                    choices=[
                        "Consulter un profil",
                        "Voir les highscores",
                        Choice(value=None,name="Quitter")
                    ],
                    default=None,
                ).execute()
                

                if actionbdd == "Consulter un profil":
                    nom_a_checker = input("Pseudo du profil à consulter :")
                    RequeteDAO().obtenir_page_joueuse(nom_a_checker) #la requête est directement gérée par la DAO

                if actionbdd == "Voir les highscores":
                    RequeteDAO().obtenir_highscores() #la requête est gérée par la DAO

                if actionbdd == None: # On sort du programme
                    partie_en_cours = False
                    break