This application is a game inspired by the French games "Pyramide" and the "Philosopher's Game".
The game is only in French.

Initialize the database with the provided init_db.sql file.
Then run main.py to launch it!

Start by creating an account, then you can play or consult the leaderboard.
The game is simple: you have to find the name of a French Wikipedia article with the help of hyperlinks on the page.
You can choose between different difficulties and different categories.
To start, we advise you to play the "Facile" (Easy) difficulty and the "Aléatoire" (Random) game mode.

Have fun!