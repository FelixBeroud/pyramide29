class Reglage:
    def __init__(self, difficulte:str = "Facile", nombre_indices_init:int = 5, nombre_tentatives:int = 7, facteur_multip:float = 1) -> None:
        self.difficulte = difficulte
        self.nombre_indices_init = nombre_indices_init
        self.nombre_tentatives = nombre_tentatives
        self.facteur_multip = facteur_multip

if __name__ == "__main__":
    moy = Reglage("Moyen", 3, 5, 1.5)
    print(moy.facteur_multip) # 1.5

    print(Reglage().difficulte) # "Facile"