"""
Classe minimale servant à faire des tests avec les classes de DAO.
À remplacer par la vraie classe Article une fois celle-ci codée.
"""
class Article:
    def __init__(self, titre:str = "Titre non renseigné", texte:str = "Texte non renseigné", lien:str = None) -> None:

        self.titre = titre
        self.texte = texte
        self.lien = lien

if __name__== "__main__":
    rio = Article(titre = "Rio de Janeiro")
    print(rio.titre)