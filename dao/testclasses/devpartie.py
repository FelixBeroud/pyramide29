"""
Classe minimale servant à faire des tests avec les classes de DAO.
À remplacer par la vraie classe Partie une fois celle-ci codée.
"""
from devlogin import Login
from devjoueuse import Joueuse
from devreglage import Reglage
from devmanche import Manche

class Partie:
    def __init__(self, joueuse:Joueuse, score_partie:float = 0.0, liste_manches:list[Manche] = None, reglage:Reglage = Reglage()) -> None:
        self.score_partie = score_partie
        self.liste_manches = liste_manches
        self.joueuse = joueuse
        self.reglage = reglage

if __name__== "__main__":
    tiphou = Joueuse(login = Login("Tiphaine", "mdp_de_tiphaine"))
    partie_legendaire = Partie(tiphou, score_partie = 999, reglage = Reglage("Difficile", 3,3,2))
    print(partie_legendaire.joueuse.login)
    print(partie_legendaire.score_partie)
    print(partie_legendaire.reglage.difficulte)
