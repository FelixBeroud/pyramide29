"""
Question : quand on fetch le résultat d'un count, obtient-on directement un entier ?
"""

from connection import DBConnection

coco = DBConnection().connection
with coco:
    with coco.cursor() as cursor:
        # cursor.execute(
        #     "SELECT COUNT(nom) FROM TablePhilosophe"
        # )
        # q = cursor.fetchall()
        # print(q[0]['count'])

        # cursor.execute(
        #     "SELECT * FROM TablePhilosophe"
        # )
        # q2 = cursor.fetchall()
        # print(len(q2))
        # print(q2[0]['date_naissance'])

        # cursor.execute(
        #     "SELECT * FROM TablePhilosophe"
        # )
        # q3 = cursor.fetchall()[0]['date_naissance']
        # print(q3)

        cursor.execute(
            "SELECT * FROM TablePhilosophe WHERE nom = 'Pascal'"
        )
        q2 = cursor.fetchall()
        print(len(q2))
