"""
Classe minimale servant à faire des tests avec les classes de DAO.
À remplacer par la vraie classe Manche une fois celle-ci codée.


Remarque importante : 
on suppose que "nombre de tentatives" représente le nombre final de tentatives qui a réellement été faites par la personne,
pas le nombre auquel elle a droit - qui est plutôt stocké dans Partie.reglage. 

Du coup, il faut une convention pour indiquer lorsqu'une partie a été échouée (on a fait toutes les tentatives et pas trouvé).
Dans les exemples sur le main_test.py j'ai choisi de mettre 8 tentatives par convention lorsqu'on a échoué.
"""
from devarticle import Article
class Manche:
    def __init__(self, article:Article = None, nombre_tentatives:int = 0, score_manche:float = 0, liste_indices_tot = None) -> None:
        self.article = article
        self.nombre_tentatives = nombre_tentatives
        self.score_manche = score_manche
        self.liste_indices_tot = liste_indices_tot

if __name__ == "__main__":
    manche1 = Manche(article = Article(titre = "Rio de Janeiro"))
    print(manche1.score_manche) # 0
    print(manche1.article.titre) # Rio de Janeiro
