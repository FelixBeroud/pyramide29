"""
Classe minimale servant à faire des tests avec les classes de DAO.
À remplacer par la vraie classe Joueuse une fois celle-ci codée.
"""
from datetime import date
from devlogin import Login

class Joueuse:
    def __init__(self, login:Login = Login("anonyme", "0000aaaa"), date_inscription = date.today(), score_acc:float = 0.0, score_moy:float = 0.0) -> None:
        self.login = login
        self.date_inscription = date_inscription
        self.score_acc = score_acc
        self.score_moy = score_moy

if __name__ == "__main__":
    grubby = Joueuse(Login("Grubby", "super_mdp_secret"))
    print(grubby.login.mdp)
    print(grubby.date_inscription)
    print(grubby.score_acc)
    print(type(grubby.score_acc))