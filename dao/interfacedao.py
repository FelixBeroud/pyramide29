import sys
sys.path.append('../pyramide29')


from dao.connection import DBConnection
from joueuse import Joueuse 
from partie import Partie
from datetime import date

class InterfaceDAO:
    """
    Classe centrale pour l'interaction avec la bdd, avec JoueuseDAO et RequeteDAO.
    Dipose uniquement de la méthode updater_bdd : prend une Partie et met à jour les entrées correspondantes sur la bdd (Partie, Manche, Joueuse).
    """

    def updater_bdd(self, game:Partie):
        """
        Méthode appelée pour mettre à jour la bdd après une partie : 
        - MAJ de la table Partie
        - MAJ de la table Manche
        - modifie le score de la joueuse sur la table Joueuse

        Cette méthode update aussi les valeurs de score_acc et score_moy de la personne qui a joué, 
        en fonction des points gagnés dans la partie.

         Attributes
        ----------
        game (Partie):
            partie dont on veut envoyer les données sur la bdd
        """
        
        # 1. Obtenir la connexion
        coco = DBConnection().connection
        with coco:
            with coco.cursor() as cursor:

        # 2. C'est quoi l'id de la joueuse
                cursor.execute("SELECT id_joueuse FROM Joueuse WHERE pseudo = %(login)s ",
                {"login" : game.joueuse.login.nom})

                        # possible d'ajouter un warning au cas où plusieurs personnes aient le même pseudo
                res = cursor.fetchall()
                if len(res) ==0:
                    raise ValueError("Erreur : la BDD ne contient aucune joueuse ayant le pseudo demandé")
                id_joueuse = res[0]['id_joueuse']
        
        # 3. Ajouter la nouvelle partie

                cursor.execute("INSERT INTO Partie (id_joueuse, date_partie, score_partie, difficulte)" \
                "VALUES (%(id_joueuse)s, %(date_partie)s, %(score_partie)s, %(difficulte)s)" \
                "RETURNING id_partie", # renvoie l'id de cette partie, qui est attribué automatiquement
                {
                    "id_joueuse" :  id_joueuse,
                    "date_partie" :  date.today(),
                    "score_partie" :  game.score_partie,
                    "difficulte" :  game.reglage.difficulte,
                }
                ,)

                id_partie = cursor.fetchall()[0]['id_partie']    

        # 4. Ajouter les nouvelles manches
                for M in game.liste_manches:
                    cursor.execute("INSERT INTO Manche (id_partie, score_manche, titre)" \
                        "VALUES (%(id_partie)s, %(score_manche)s, %(titre)s)" \
                        "RETURNING id_partie", # renvoie l'id de cette partie, qui est attribué automatiquement
                        {
                            "id_partie" :  id_partie,
                            "score_manche" :  M.score_manche,
                            "titre" :  M.article.titre
                        }
                        ,)

        # 5. MAJ le profil de la joueuse
                cursor.execute(
                    "SELECT nombre_parties FROM Joueuse WHERE pseudo = %(nom)s ",
                    {
                        "nom" : game.joueuse.login.nom
                    }
                )
                scores = cursor.fetchall()[0]
                n = scores['nombre_parties'] + 1

                ### UPDATE LOCALE
                game.joueuse.score_acc = game.joueuse.score_acc + game.score_partie
                game.joueuse.score_moy = game.joueuse.score_acc/n
                
                cursor.execute(
                    "UPDATE Joueuse " \
                    "SET score_total = %(score_acc)s, score_moyen = %(score_moy)s, nombre_parties = %(nb_p)s" \
                    "WHERE pseudo = %(nom)s "
                ,
                {
                    "nom" : game.joueuse.login.nom,
                    "score_acc" : game.joueuse.score_acc,
                    "score_moy" : game.joueuse.score_moy,
                    "nb_p" : n
                })
                    
# Test : 
# cf. dao_test.py


                    
            

