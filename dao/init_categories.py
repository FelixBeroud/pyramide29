"""
Ce script, ainsi que celui de init_bdd.py, ne sont à exécuter SEULEMENT si les tables n'ont pas été créées avec le fichier init_db.sql fourni.
Ce script est à exécuter une seule fois (mais l'exécuter plusieurs fois n'est pas grave, c'est juste inutile).
Il initialise la table (séparée du reste de la base de données) des catégories.
"""
import sys
sys.path.append('../pyramide29')
from connection import DBConnection
coco = DBConnection().connection

with coco:
    with coco.cursor() as cursor:
        cursor.execute(
            "DROP TABLE IF EXISTS Articles ; \
            CREATE TABLE Articles (titre_complet varchar NOT NULL, \
            categorie varchar NOT NULL\
            ); \
            INSERT INTO Articles VALUES ('Paris', 'Villes'); \
INSERT INTO Articles VALUES ('Toulouse', 'Villes'); \
INSERT INTO Articles VALUES ('Rennes', 'Villes'); \
INSERT INTO Articles VALUES ('Strasbourg', 'Villes'); \
INSERT INTO Articles VALUES ('Marseille', 'Villes'); \
INSERT INTO Articles VALUES ('Nice', 'Villes'); \
INSERT INTO Articles VALUES ('Bordeaux', 'Villes'); \
INSERT INTO Articles VALUES ('Lille', 'Villes'); \
INSERT INTO Articles VALUES ('Caen', 'Villes'); \
INSERT INTO Articles VALUES ('Le Havre', 'Villes'); \
INSERT INTO Articles VALUES ('Nantes', 'Villes'); \
INSERT INTO Articles VALUES ('Perpignan', 'Villes'); \
INSERT INTO Articles VALUES ('Montpellier', 'Villes'); \
INSERT INTO Articles VALUES ('Lyon', 'Villes'); \
INSERT INTO Articles VALUES ('Reims', 'Villes'); \
INSERT INTO Articles VALUES ('Orléans', 'Villes'); \
INSERT INTO Articles VALUES ('Nancy', 'Villes'); \
INSERT INTO Articles VALUES ('Brest', 'Villes'); \
INSERT INTO Articles VALUES ('La Rochelle', 'Villes'); \
INSERT INTO Articles VALUES ('Clermont-Ferrand', 'Villes'); \
INSERT INTO Articles VALUES ('Versailles', 'Villes'); \
INSERT INTO Articles VALUES ('Berlin', 'Villes'); \
INSERT INTO Articles VALUES ('Londres', 'Villes'); \
INSERT INTO Articles VALUES ('Dublin', 'Villes'); \
INSERT INTO Articles VALUES ('Moscou', 'Villes'); \
INSERT INTO Articles VALUES ('Le Caire', 'Villes'); \
INSERT INTO Articles VALUES ('Pékin', 'Villes'); \
INSERT INTO Articles VALUES ('Tokyo', 'Villes'); \
INSERT INTO Articles VALUES ('Jakarta', 'Villes'); \
INSERT INTO Articles VALUES ('Rio de Janeiro', 'Villes'); \
INSERT INTO Articles VALUES ('Toulon', 'Villes'); \
INSERT INTO Articles VALUES ('New York', 'Villes'); \
INSERT INTO Articles VALUES ('Los Angeles', 'Villes'); \
INSERT INTO Articles VALUES ('Las Vegas', 'Villes'); \
INSERT INTO Articles VALUES ('Mexico City', 'Villes'); \
INSERT INTO Articles VALUES ('Marrakech', 'Villes'); \
INSERT INTO Articles VALUES ('Istanbul', 'Villes'); \
INSERT INTO Articles VALUES ('Rome', 'Villes'); \
INSERT INTO Articles VALUES ('Naples', 'Villes'); \
INSERT INTO Articles VALUES ('Venise', 'Villes'); \
INSERT INTO Articles VALUES ('Monaco', 'Villes'); \
INSERT INTO Articles VALUES ('Madrid', 'Villes'); \
INSERT INTO Articles VALUES ('Barcelone', 'Villes'); \
INSERT INTO Articles VALUES ('Lisbonne', 'Villes'); \
INSERT INTO Articles VALUES ('Athènes', 'Villes'); \
INSERT INTO Articles VALUES ('Stockholm', 'Villes'); \
INSERT INTO Articles VALUES ('Oslo', 'Villes'); \
INSERT INTO Articles VALUES ('Calcutta', 'Villes'); \
INSERT INTO Articles VALUES ('Bombay', 'Villes'); \
INSERT INTO Articles VALUES ('Shanghai', 'Villes'); \
INSERT INTO Articles VALUES ('Séoul', 'Villes'); \
INSERT INTO Articles VALUES ('Manille', 'Villes'); \
INSERT INTO Articles VALUES ('Sydney', 'Villes'); \
INSERT INTO Articles VALUES ('Melbourne', 'Villes'); \
INSERT INTO Articles VALUES ('Buenos Aires', 'Villes'); \
INSERT INTO Articles VALUES ('Dallas', 'Villes'); \
INSERT INTO Articles VALUES ('Boston', 'Villes'); \
INSERT INTO Articles VALUES ('Philadelphie', 'Villes'); \
INSERT INTO Articles VALUES ('Vancouver', 'Villes'); \
INSERT INTO Articles VALUES ('Montréal', 'Villes'); \
INSERT INTO Articles VALUES ('Toronto', 'Villes'); \
INSERT INTO Articles VALUES ('Bruxelles', 'Villes'); \
INSERT INTO Articles VALUES ('Amsterdam', 'Villes'); \
INSERT INTO Articles VALUES ('Rotterdam', 'Villes'); \
INSERT INTO Articles VALUES ('Grenoble', 'Villes'); \
INSERT INTO Articles VALUES ('Dijon', 'Villes'); \
INSERT INTO Articles VALUES ('Mulhouse', 'Villes'); \
INSERT INTO Articles VALUES ('Rouen', 'Villes'); \
INSERT INTO Articles VALUES ('Pontoise', 'Villes'); \
INSERT INTO Articles VALUES ('Poitiers', 'Villes'); \
INSERT INTO Articles VALUES ('Tours', 'Villes'); \
INSERT INTO Articles VALUES ('Amiens', 'Villes'); \
INSERT INTO Articles VALUES ('Édimbourg', 'Villes'); \
INSERT INTO Articles VALUES ('Manchester', 'Villes'); \
INSERT INTO Articles VALUES ('Liverpool', 'Villes'); \
INSERT INTO Articles VALUES ('Bangkok', 'Villes'); \
INSERT INTO Articles VALUES ('Jérusalem', 'Villes'); \
INSERT INTO Articles VALUES ('Budapest', 'Villes'); \
INSERT INTO Articles VALUES ('Vienne', 'Villes'); \
INSERT INTO Articles VALUES ('Salzbourg', 'Villes'); \
INSERT INTO Articles VALUES ('Munich', 'Villes'); \
INSERT INTO Articles VALUES ('Hanovre', 'Villes'); \
INSERT INTO Articles VALUES ('Miami', 'Villes'); \
INSERT INTO Articles VALUES ('Varsovie', 'Villes'); \
INSERT INTO Articles VALUES ('Prague', 'Villes'); \
INSERT INTO Articles VALUES ('Genève', 'Villes'); \
INSERT INTO Articles VALUES ('Milan', 'Villes'); \
INSERT INTO Articles VALUES ('Turin', 'Villes'); \
INSERT INTO Articles VALUES ('Ajaccio', 'Villes'); \
INSERT INTO Articles VALUES ('Johannesburg', 'Villes'); \
INSERT INTO Articles VALUES ('Dakar', 'Villes'); \
INSERT INTO Articles VALUES ('Abidjan', 'Villes'); \
INSERT INTO Articles VALUES ('Casablanca', 'Villes'); \
INSERT INTO Articles VALUES ('Bamako', 'Villes'); \
INSERT INTO Articles VALUES ('Tunis', 'Villes'); \
INSERT INTO Articles VALUES ('Alger', 'Villes'); \
INSERT INTO Articles VALUES ('Saint-Étienne', 'Villes'); \
INSERT INTO Articles VALUES ('Saint-Pétersbourg', 'Villes'); \
INSERT INTO Articles VALUES ('Copenhague', 'Villes'); \
INSERT INTO Articles VALUES ('La Havane', 'Villes'); \
INSERT INTO Articles VALUES ('Afghanistan', 'Pays'); \
INSERT INTO Articles VALUES ('Afrique du Sud', 'Pays'); \
INSERT INTO Articles VALUES ('Albanie', 'Pays'); \
INSERT INTO Articles VALUES ('Algérie', 'Pays'); \
INSERT INTO Articles VALUES ('Allemagne', 'Pays'); \
INSERT INTO Articles VALUES ('Arabie Saoudite', 'Pays'); \
INSERT INTO Articles VALUES ('Argentine', 'Pays'); \
INSERT INTO Articles VALUES ('Australie', 'Pays'); \
INSERT INTO Articles VALUES ('Autriche', 'Pays'); \
INSERT INTO Articles VALUES ('Bahamas', 'Pays'); \
INSERT INTO Articles VALUES ('Bangladesh', 'Pays'); \
INSERT INTO Articles VALUES ('Belgique', 'Pays'); \
INSERT INTO Articles VALUES ('Biélorussie', 'Pays'); \
INSERT INTO Articles VALUES ('Bolivie', 'Pays'); \
INSERT INTO Articles VALUES ('Brésil', 'Pays'); \
INSERT INTO Articles VALUES ('Bulgarie', 'Pays'); \
INSERT INTO Articles VALUES ('Cambodge', 'Pays'); \
INSERT INTO Articles VALUES ('Cameroun', 'Pays'); \
INSERT INTO Articles VALUES ('Canada', 'Pays'); \
INSERT INTO Articles VALUES ('Chili', 'Pays'); \
INSERT INTO Articles VALUES ('Chine', 'Pays'); \
INSERT INTO Articles VALUES ('Chypre', 'Pays'); \
INSERT INTO Articles VALUES ('Colombie', 'Pays'); \
INSERT INTO Articles VALUES ('Corée du Nord', 'Pays'); \
INSERT INTO Articles VALUES ('Corée du Sud', 'Pays'); \
INSERT INTO Articles VALUES ('Costa Rica', 'Pays'); \
INSERT INTO Articles VALUES ('Croatie', 'Pays'); \
INSERT INTO Articles VALUES ('Cuba', 'Pays'); \
INSERT INTO Articles VALUES ('Danemark', 'Pays'); \
INSERT INTO Articles VALUES ('Égypte', 'Pays'); \
INSERT INTO Articles VALUES ('Équateur', 'Pays'); \
INSERT INTO Articles VALUES ('Espagne', 'Pays'); \
INSERT INTO Articles VALUES ('Estonie', 'Pays'); \
INSERT INTO Articles VALUES ('États-Unis', 'Pays'); \
INSERT INTO Articles VALUES ('Éthiopie', 'Pays'); \
INSERT INTO Articles VALUES ('Finlande', 'Pays'); \
INSERT INTO Articles VALUES ('France', 'Pays'); \
INSERT INTO Articles VALUES ('Géorgie', 'Pays'); \
INSERT INTO Articles VALUES ('Ghana', 'Pays'); \
INSERT INTO Articles VALUES ('Grèce', 'Pays'); \
INSERT INTO Articles VALUES ('Guatemala', 'Pays'); \
INSERT INTO Articles VALUES ('Honduras', 'Pays'); \
INSERT INTO Articles VALUES ('Hongrie', 'Pays'); \
INSERT INTO Articles VALUES ('Inde', 'Pays'); \
INSERT INTO Articles VALUES ('Indonésie', 'Pays'); \
INSERT INTO Articles VALUES ('Iran', 'Pays'); \
INSERT INTO Articles VALUES ('Irlande', 'Pays'); \
INSERT INTO Articles VALUES ('Islande', 'Pays'); \
INSERT INTO Articles VALUES ('Israël', 'Pays'); \
INSERT INTO Articles VALUES ('Italie', 'Pays'); \
INSERT INTO Articles VALUES ('Japon', 'Pays'); \
INSERT INTO Articles VALUES ('Jordanie', 'Pays'); \
INSERT INTO Articles VALUES ('Kazakhstan', 'Pays'); \
INSERT INTO Articles VALUES ('Kenya', 'Pays'); \
INSERT INTO Articles VALUES ('Liban', 'Pays'); \
INSERT INTO Articles VALUES ('Lituanie', 'Pays'); \
INSERT INTO Articles VALUES ('Luxembourg', 'Pays'); \
INSERT INTO Articles VALUES ('Malaisie', 'Pays'); \
INSERT INTO Articles VALUES ('Mali', 'Pays'); \
INSERT INTO Articles VALUES ('Maroc', 'Pays'); \
INSERT INTO Articles VALUES ('Mexique', 'Pays'); \
INSERT INTO Articles VALUES ('Monaco', 'Pays'); \
INSERT INTO Articles VALUES ('Mongolie', 'Pays'); \
INSERT INTO Articles VALUES ('Népal', 'Pays'); \
INSERT INTO Articles VALUES ('Niger', 'Pays'); \
INSERT INTO Articles VALUES ('Nigéria', 'Pays'); \
INSERT INTO Articles VALUES ('Norvège', 'Pays'); \
INSERT INTO Articles VALUES ('Nouvelle-Zélande', 'Pays'); \
INSERT INTO Articles VALUES ('Pakistan', 'Pays'); \
INSERT INTO Articles VALUES ('Panama', 'Pays'); \
INSERT INTO Articles VALUES ('Pays-Bas', 'Pays'); \
INSERT INTO Articles VALUES ('Pérou', 'Pays'); \
INSERT INTO Articles VALUES ('Portugal', 'Pays'); \
INSERT INTO Articles VALUES ('Pologne', 'Pays'); \
INSERT INTO Articles VALUES ('Philippines', 'Pays'); \
INSERT INTO Articles VALUES ('Qatar', 'Pays'); \
INSERT INTO Articles VALUES ('République Tchèque', 'Pays'); \
INSERT INTO Articles VALUES ('Royaume-Uni', 'Pays'); \
INSERT INTO Articles VALUES ('Roumanie', 'Pays'); \
INSERT INTO Articles VALUES ('Russie', 'Pays'); \
INSERT INTO Articles VALUES ('Sénégal', 'Pays'); \
INSERT INTO Articles VALUES ('Serbie', 'Pays'); \
INSERT INTO Articles VALUES ('Seychelles', 'Pays'); \
INSERT INTO Articles VALUES ('Singapour', 'Pays'); \
INSERT INTO Articles VALUES ('Slovaquie', 'Pays'); \
INSERT INTO Articles VALUES ('Slovénie', 'Pays'); \
INSERT INTO Articles VALUES ('Somalie', 'Pays'); \
INSERT INTO Articles VALUES ('Soudan', 'Pays'); \
INSERT INTO Articles VALUES ('Suède', 'Pays'); \
INSERT INTO Articles VALUES ('Suisse', 'Pays'); \
INSERT INTO Articles VALUES ('Syrie', 'Pays'); \
INSERT INTO Articles VALUES ('Tanzanie', 'Pays'); \
INSERT INTO Articles VALUES ('Tchad', 'Pays'); \
INSERT INTO Articles VALUES ('Thaïlande', 'Pays'); \
INSERT INTO Articles VALUES ('Tunisie', 'Pays'); \
INSERT INTO Articles VALUES ('Turquie', 'Pays'); \
INSERT INTO Articles VALUES ('Ukraine', 'Pays'); \
INSERT INTO Articles VALUES ('Uruguay', 'Pays'); \
INSERT INTO Articles VALUES ('Venezuela', 'Pays'); \
INSERT INTO Articles VALUES ('Vietnam', 'Pays'); \
INSERT INTO Articles VALUES ('Lion', 'Animaux'); \
INSERT INTO Articles VALUES ('Tigre', 'Animaux'); \
INSERT INTO Articles VALUES ('Léopard', 'Animaux'); \
INSERT INTO Articles VALUES ('Baleine', 'Animaux'); \
INSERT INTO Articles VALUES ('Requin', 'Animaux'); \
INSERT INTO Articles VALUES ('Tortue', 'Animaux'); \
INSERT INTO Articles VALUES ('Hérisson', 'Animaux'); \
INSERT INTO Articles VALUES ('Pingouin', 'Animaux'); \
INSERT INTO Articles VALUES ('Perroquet', 'Animaux'); \
INSERT INTO Articles VALUES ('Poisson-clown', 'Animaux'); \
INSERT INTO Articles VALUES ('Kangourou', 'Animaux'); \
INSERT INTO Articles VALUES ('Koala', 'Animaux'); \
INSERT INTO Articles VALUES ('Colombe', 'Animaux'); \
INSERT INTO Articles VALUES ('Canard', 'Animaux'); \
INSERT INTO Articles VALUES ('Pieuvre', 'Animaux'); \
INSERT INTO Articles VALUES ('Fourmi', 'Animaux'); \
INSERT INTO Articles VALUES ('Abeille', 'Animaux'); \
INSERT INTO Articles VALUES ('Guêpe', 'Animaux'); \
INSERT INTO Articles VALUES ('Jaguar', 'Animaux'); \
INSERT INTO Articles VALUES ('Guépard', 'Animaux'); \
INSERT INTO Articles VALUES ('Ornithorynque', 'Animaux'); \
INSERT INTO Articles VALUES ('Dromadaire', 'Animaux'); \
INSERT INTO Articles VALUES ('Autruche', 'Animaux'); \
INSERT INTO Articles VALUES ('Mouche', 'Animaux'); \
INSERT INTO Articles VALUES ('Paon', 'Animaux'); \
INSERT INTO Articles VALUES ('Sanglier', 'Animaux'); \
INSERT INTO Articles VALUES ('Chat', 'Animaux'); \
INSERT INTO Articles VALUES ('Chien', 'Animaux'); \
INSERT INTO Articles VALUES ('Renard', 'Animaux'); \
INSERT INTO Articles VALUES ('Moustique', 'Animaux'); \
INSERT INTO Articles VALUES ('Rat', 'Animaux'); \
INSERT INTO Articles VALUES ('Sauterelle (insecte)', 'Animaux'); \
INSERT INTO Articles VALUES ('Bison', 'Animaux'); \
INSERT INTO Articles VALUES ('Oie', 'Animaux'); \
INSERT INTO Articles VALUES ('Poule', 'Animaux'); \
INSERT INTO Articles VALUES ('Taupe', 'Animaux'); \
INSERT INTO Articles VALUES ('Saumon', 'Animaux'); \
INSERT INTO Articles VALUES ('Dauphin', 'Animaux'); \
INSERT INTO Articles VALUES ('Escargot', 'Animaux'); \
INSERT INTO Articles VALUES ('Limace', 'Animaux'); \
INSERT INTO Articles VALUES ('Crapaud', 'Animaux'); \
INSERT INTO Articles VALUES ('Étoile de mer', 'Animaux'); \
INSERT INTO Articles VALUES ('Lama (animal)', 'Animaux'); \
INSERT INTO Articles VALUES ('Lapin', 'Animaux'); \
INSERT INTO Articles VALUES ('Girafe', 'Animaux'); \
INSERT INTO Articles VALUES ('Rhinocéros', 'Animaux'); \
INSERT INTO Articles VALUES ('Crocodile', 'Animaux'); \
INSERT INTO Articles VALUES ('Chimpanzé', 'Animaux'); \
INSERT INTO Articles VALUES ('Gorille', 'Animaux'); \
INSERT INTO Articles VALUES ('Porc', 'Animaux'); \
INSERT INTO Articles VALUES ('Marmotte', 'Animaux'); \
INSERT INTO Articles VALUES ('Écureuil', 'Animaux'); \
INSERT INTO Articles VALUES ('Aigle', 'Animaux'); \
INSERT INTO Articles VALUES ('Vautour', 'Animaux'); \
INSERT INTO Articles VALUES ('Éléphant', 'Animaux'); \
INSERT INTO Articles VALUES ('Âne', 'Animaux'); \
INSERT INTO Articles VALUES ('Alligator', 'Animaux'); \
INSERT INTO Articles VALUES ('Morue', 'Animaux'); \
INSERT INTO Articles VALUES ('Zèbre', 'Animaux'); \
INSERT INTO Articles VALUES ('Puma', 'Animaux'); \
INSERT INTO Articles VALUES ('Suricate', 'Animaux'); \
INSERT INTO Articles VALUES ('Lézard', 'Animaux'); \
INSERT INTO Articles VALUES ('Crevette', 'Animaux'); \
INSERT INTO Articles VALUES ('Mante religieuse', 'Animaux'); \
INSERT INTO Articles VALUES ('Sardine', 'Animaux'); \
INSERT INTO Articles VALUES ('Huître', 'Animaux'); \
INSERT INTO Articles VALUES ('Crabe', 'Animaux'); \
INSERT INTO Articles VALUES ('Chenille (lépidoptère)', 'Animaux'); \
INSERT INTO Articles VALUES ('Mouton', 'Animaux'); \
INSERT INTO Articles VALUES ('Cheval', 'Animaux'); \
INSERT INTO Articles VALUES ('Raton laveur', 'Animaux'); \
INSERT INTO Articles VALUES ('Cygne', 'Animaux'); \
INSERT INTO Articles VALUES ('Furet', 'Animaux'); \
INSERT INTO Articles VALUES ('Coucou', 'Animaux'); \
INSERT INTO Articles VALUES ('Ouistiti', 'Animaux'); \
INSERT INTO Articles VALUES ('Couleuvre', 'Animaux'); \
INSERT INTO Articles VALUES ('Gazelle', 'Animaux'); \
INSERT INTO Articles VALUES ('Chevreuil', 'Animaux'); \
INSERT INTO Articles VALUES ('Corbeau', 'Animaux'); \
INSERT INTO Articles VALUES ('Mouette', 'Animaux'); \
INSERT INTO Articles VALUES ('Castor (genre)', 'Animaux'); \
INSERT INTO Articles VALUES ('Cigogne', 'Animaux'); \
INSERT INTO Articles VALUES ('Lynx', 'Animaux'); \
INSERT INTO Articles VALUES ('Babouin', 'Animaux'); \
INSERT INTO Articles VALUES ('Hamster', 'Animaux'); \
INSERT INTO Articles VALUES ('Chèvre', 'Animaux'); \
INSERT INTO Articles VALUES ('Tique', 'Animaux'); \
INSERT INTO Articles VALUES ('Bonobo', 'Animaux'); \
INSERT INTO Articles VALUES ('Orang-outan', 'Animaux'); \
INSERT INTO Articles VALUES ('Thon', 'Animaux'); \
INSERT INTO Articles VALUES ('Paresseux', 'Animaux'); \
INSERT INTO Articles VALUES ('Gnou', 'Animaux'); \
INSERT INTO Articles VALUES ('Lamantin', 'Animaux'); \
INSERT INTO Articles VALUES ('Hirondelle', 'Animaux'); \
INSERT INTO Articles VALUES ('Licorne', 'Animaux'); \
INSERT INTO Articles VALUES ('Canari', 'Animaux'); \
INSERT INTO Articles VALUES ('Chouette', 'Animaux'); \
INSERT INTO Articles VALUES ('Zébu', 'Animaux'); \
INSERT INTO Articles VALUES ('Orque', 'Animaux'); \
INSERT INTO Articles VALUES ('Loutre', 'Animaux'); \
INSERT INTO Articles VALUES ('Lionel Messi', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Cristiano Ronaldo', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Zinédine Zidane', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Thierry Henry', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Rafael Nadal', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Novak Djokovic', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Roger Federer', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Laure Manaudou', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Florent Manaudou', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Karim Benzema', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Tony Parker', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Mike Tyson', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Tiger Woods', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('LeBron James', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Usain Bolt', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Neymar', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Serena Williams', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Michael Jordan', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Diego Maradona', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Michael Phelps', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Yannick Noah', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Pelé', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Teddy Riner', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('David Beckham', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Mohamed Ali', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Lewis Hamilton', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Michael Schumacher', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Ronaldinho', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Andy Murray', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Richard Gasquet', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Jo-Wilfried Tsonga', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Paul Pogba', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Antoine Griezmann', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('François Hollande', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Nicolas Sarkozy', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Emmanuel Macron', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Donald Trump', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Abraham Lincoln', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Marilyn Monroe', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Élisabeth II', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Neil Armstrong', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Louis XIV', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Louis XVI', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Charlemagne', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Napoléon Ier', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Jules César', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Louis Pasteur', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Albert Einstein', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Léonard de Vinci', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Marco Polo', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Marcel Proust', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Victor Hugo', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Jean-Paul II', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Barack Obama', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Gengis Khan', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Vladimir Poutine', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Émile Zola', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Charles de Gaulle', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Jean Jaurès', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Molière', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Pablo Picasso', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Charles Baudelaire', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Bruce Lee', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Alexandre le Grand', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Alexandre Dumas', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Isaac Newton', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Christophe Colomb', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Marie Curie', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Aristote', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Socrate', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Platon', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Charles Darwin', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('François Mitterrand', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Karl Marx', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('William Shakespeare', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('René Descartes', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Michel-Ange', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Voltaire', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Clovis', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Galilée (savant)', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Bill Gates', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Pythagore', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Charlie Chaplin', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Alfred Hitchcock', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Walt Disney', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Rembrandt', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Vincent Van Gogh', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Arthur Rimbaud', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Bob Marley', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Elvis Presley', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Spartacus', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Madonna', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Rihanna', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Beyoncé', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Michael Jackson', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('François Ier (roi de France)', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Kylian Mbappé', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Jacques Chirac', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Jackie Chan', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Nelson Mandela', 'Personnes célèbres'); \
INSERT INTO Articles VALUES ('Carotte', 'Divers'); \
INSERT INTO Articles VALUES ('Philosophie', 'Divers'); \
INSERT INTO Articles VALUES ('Gouttière (architecture)', 'Divers'); \
INSERT INTO Articles VALUES ('Assiette (vaisselle)', 'Divers'); \
INSERT INTO Articles VALUES ('Métro', 'Divers'); \
INSERT INTO Articles VALUES ('Pyramide', 'Divers'); \
INSERT INTO Articles VALUES ('Hélicoptère', 'Divers'); \
INSERT INTO Articles VALUES ('Fusil', 'Divers'); \
INSERT INTO Articles VALUES ('Première Guerre Mondiale', 'Divers'); \
INSERT INTO Articles VALUES ('Harry Potter (série de films)', 'Divers'); \
INSERT INTO Articles VALUES ('Google', 'Divers'); \
INSERT INTO Articles VALUES ('Metallica', 'Divers'); \
INSERT INTO Articles VALUES ('Pokémon', 'Divers'); \
INSERT INTO Articles VALUES ('Hulk', 'Divers'); \
INSERT INTO Articles VALUES ('Facebook', 'Divers'); \
INSERT INTO Articles VALUES ('Football', 'Divers'); \
INSERT INTO Articles VALUES ('Soleil', 'Divers'); \
INSERT INTO Articles VALUES ('Traduction', 'Divers'); \
INSERT INTO Articles VALUES ('Ordinateur', 'Divers'); \
INSERT INTO Articles VALUES ('Jupiter (planète)', 'Divers'); \
INSERT INTO Articles VALUES ('Cryptomonnaie', 'Divers'); \
INSERT INTO Articles VALUES ('Aluminium', 'Divers'); \
INSERT INTO Articles VALUES ('Jambon', 'Divers'); \
INSERT INTO Articles VALUES ('Bicyclette', 'Divers'); \
INSERT INTO Articles VALUES ('Abricot', 'Divers'); \
INSERT INTO Articles VALUES ('Xylophone', 'Divers'); \
INSERT INTO Articles VALUES ('Photographie', 'Divers'); \
INSERT INTO Articles VALUES ('Athlétisme', 'Divers'); \
INSERT INTO Articles VALUES ('Batman', 'Divers'); \
INSERT INTO Articles VALUES ('Cantine', 'Divers'); \
INSERT INTO Articles VALUES ('Boisson', 'Divers'); \
INSERT INTO Articles VALUES ('Champignon', 'Divers'); \
INSERT INTO Articles VALUES ('Poésie', 'Divers'); \
INSERT INTO Articles VALUES ('Scie', 'Divers'); \
INSERT INTO Articles VALUES ('Mètre', 'Divers'); \
INSERT INTO Articles VALUES ('Biberon', 'Divers'); \
INSERT INTO Articles VALUES ('Grippe', 'Divers'); \
INSERT INTO Articles VALUES ('Miroir', 'Divers'); \
INSERT INTO Articles VALUES ('Sorbet', 'Divers'); \
INSERT INTO Articles VALUES ('Menthe', 'Divers'); \
INSERT INTO Articles VALUES ('Chirurgie', 'Divers'); \
INSERT INTO Articles VALUES ('Ingénieur', 'Divers'); \
INSERT INTO Articles VALUES ('Développement durable', 'Divers'); \
INSERT INTO Articles VALUES ('Environnement', 'Divers'); \
INSERT INTO Articles VALUES ('Astronomie', 'Divers'); \
INSERT INTO Articles VALUES ('Tournevis', 'Divers'); \
INSERT INTO Articles VALUES ('Croix', 'Divers'); \
INSERT INTO Articles VALUES ('Cimetière', 'Divers'); \
INSERT INTO Articles VALUES ('Logiciel', 'Divers'); \
INSERT INTO Articles VALUES ('Supermarché', 'Divers'); \
INSERT INTO Articles VALUES ('Oreiller', 'Divers'); \
INSERT INTO Articles VALUES ('Nuit', 'Divers'); \
INSERT INTO Articles VALUES ('Sarcophage', 'Divers'); \
INSERT INTO Articles VALUES ('Loi', 'Divers'); \
INSERT INTO Articles VALUES ('TGV', 'Divers'); \
INSERT INTO Articles VALUES ('Café', 'Divers'); \
INSERT INTO Articles VALUES ('Crèche (enfant)', 'Divers'); \
INSERT INTO Articles VALUES ('Raclette', 'Divers'); \
INSERT INTO Articles VALUES ('Spaghetti', 'Divers'); \
INSERT INTO Articles VALUES ('Sauce bolognaise', 'Divers'); \
INSERT INTO Articles VALUES ('Code pénal (France)', 'Divers'); \
INSERT INTO Articles VALUES ('Site web', 'Divers'); \
INSERT INTO Articles VALUES ('Le Monde', 'Divers'); \
INSERT INTO Articles VALUES ('Journalisme', 'Divers'); \
INSERT INTO Articles VALUES ('Yaourt', 'Divers'); \
INSERT INTO Articles VALUES ('Cancer', 'Divers'); \
INSERT INTO Articles VALUES ('Algorithme', 'Divers'); \
INSERT INTO Articles VALUES ('Théorème de Pythagore', 'Divers'); \
INSERT INTO Articles VALUES ('Hygiène', 'Divers'); \
INSERT INTO Articles VALUES ('Parfum', 'Divers'); \
INSERT INTO Articles VALUES ('Science', 'Divers'); \
INSERT INTO Articles VALUES ('Science-fiction', 'Divers'); \
INSERT INTO Articles VALUES ('Diamant', 'Divers'); \
INSERT INTO Articles VALUES ('Nageoire', 'Divers'); \
INSERT INTO Articles VALUES ('Transport', 'Divers'); \
INSERT INTO Articles VALUES ('Tourisme', 'Divers'); \
INSERT INTO Articles VALUES ('Machine à vapeur', 'Divers'); \
INSERT INTO Articles VALUES ('Locomotive', 'Divers'); \
INSERT INTO Articles VALUES ('Poker', 'Divers'); \
INSERT INTO Articles VALUES ('Noyade', 'Divers'); \
INSERT INTO Articles VALUES ('Bière', 'Divers'); \
INSERT INTO Articles VALUES ('Riz', 'Divers'); \
INSERT INTO Articles VALUES ('Himalaya', 'Divers'); \
INSERT INTO Articles VALUES ('Balai', 'Divers'); \
INSERT INTO Articles VALUES ('Tapis', 'Divers'); \
INSERT INTO Articles VALUES ('Écurie', 'Divers'); \
INSERT INTO Articles VALUES ('Amour', 'Divers'); \
INSERT INTO Articles VALUES ('Nature', 'Divers'); \
INSERT INTO Articles VALUES ('Planète', 'Divers'); \
INSERT INTO Articles VALUES ('Antiquité', 'Divers'); \
INSERT INTO Articles VALUES ('Désert', 'Divers'); \
INSERT INTO Articles VALUES ('Sahara', 'Divers'); \
INSERT INTO Articles VALUES ('Empire ottoman', 'Divers'); \
INSERT INTO Articles VALUES ('Commerce', 'Divers'); \
INSERT INTO Articles VALUES ('Bretagne', 'Divers'); \
INSERT INTO Articles VALUES ('Enchère', 'Divers'); \
INSERT INTO Articles VALUES ('Marathon (sport)', 'Divers'); \
INSERT INTO Articles VALUES ('Grue (engin)', 'Divers'); \
INSERT INTO Articles VALUES ('Pantalon', 'Divers'); \
INSERT INTO Articles VALUES ('Roue', 'Divers'); \
INSERT INTO Articles VALUES ('Silex', 'Divers'); \
INSERT INTO Articles VALUES ('Renault', 'Divers'); \
INSERT INTO Articles VALUES ('Noël', 'Divers'); \
INSERT INTO Articles VALUES ('Pâques', 'Divers'); \
INSERT INTO Articles VALUES ('Sorcier', 'Divers'); \
INSERT INTO Articles VALUES ('Révolution française', 'Divers'); \
INSERT INTO Articles VALUES ('Guillotine', 'Divers'); \
INSERT INTO Articles VALUES ('Torche', 'Divers'); \
INSERT INTO Articles VALUES ('Big Ben', 'Divers'); \
INSERT INTO Articles VALUES ('Tour Eiffel', 'Divers'); \
INSERT INTO Articles VALUES ('Seine', 'Divers'); \
INSERT INTO Articles VALUES ('Architecte', 'Divers'); \
INSERT INTO Articles VALUES ('Maçonnerie', 'Divers'); \
INSERT INTO Articles VALUES ('Phare', 'Divers'); \
INSERT INTO Articles VALUES ('Gouvernail', 'Divers'); \
INSERT INTO Articles VALUES ('Tabac', 'Divers'); \
INSERT INTO Articles VALUES ('Printemps', 'Divers'); \
INSERT INTO Articles VALUES ('Hiver', 'Divers'); \
INSERT INTO Articles VALUES ('Alpes', 'Divers'); \
INSERT INTO Articles VALUES ('Sacre', 'Divers'); \
INSERT INTO Articles VALUES ('Cathédrale', 'Divers'); \
INSERT INTO Articles VALUES ('Pollution', 'Divers'); \
INSERT INTO Articles VALUES ('Biologie', 'Divers'); \
INSERT INTO Articles VALUES ('Machine à écrire', 'Divers'); \
INSERT INTO Articles VALUES ('Tronçonneuse', 'Divers'); \
INSERT INTO Articles VALUES ('Vinaigre', 'Divers'); \
INSERT INTO Articles VALUES ('Foie', 'Divers'); \
INSERT INTO Articles VALUES ('Sang', 'Divers'); \
INSERT INTO Articles VALUES ('Homo sapiens', 'Divers'); \
INSERT INTO Articles VALUES ('Musique', 'Divers'); \
INSERT INTO Articles VALUES ('Poste', 'Divers'); \
INSERT INTO Articles VALUES ('Chaise', 'Divers'); \
INSERT INTO Articles VALUES ('Choucroute', 'Divers'); \
INSERT INTO Articles VALUES ('Beurre', 'Divers'); \
INSERT INTO Articles VALUES ('Vote', 'Divers'); \
INSERT INTO Articles VALUES ('Abstention', 'Divers'); \
INSERT INTO Articles VALUES ('Molécule', 'Divers'); \
INSERT INTO Articles VALUES ('Théorie de la mesure', 'Divers'); \
INSERT INTO Articles VALUES ('Raisin', 'Divers'); \
INSERT INTO Articles VALUES ('Librairie', 'Divers'); \
INSERT INTO Articles VALUES ('Traîneau', 'Divers'); \
INSERT INTO Articles VALUES ('Chêne', 'Divers'); \
INSERT INTO Articles VALUES ('Confiture', 'Divers'); \
INSERT INTO Articles VALUES ('Antibiotique', 'Divers'); \
INSERT INTO Articles VALUES ('Hypnose', 'Divers'); \
INSERT INTO Articles VALUES ('Bataille de Verdun', 'Divers'); \
INSERT INTO Articles VALUES ('Porte (architecture)', 'Divers'); \
INSERT INTO Articles VALUES ('Télévision', 'Divers'); \
INSERT INTO Articles VALUES ('Yoga', 'Divers'); \
INSERT INTO Articles VALUES ('Brûlure', 'Divers'); \
INSERT INTO Articles VALUES ('Hellfest', 'Divers'); \
INSERT INTO Articles VALUES ('Prénom', 'Divers'); \
INSERT INTO Articles VALUES ('Autoroute', 'Divers'); \
INSERT INTO Articles VALUES ('Moto', 'Divers'); \
INSERT INTO Articles VALUES ('Scooter', 'Divers'); \
INSERT INTO Articles VALUES ('Pomme de terre', 'Divers'); \
INSERT INTO Articles VALUES ('Égout', 'Divers'); \
INSERT INTO Articles VALUES ('Cupcake', 'Divers'); \
INSERT INTO Articles VALUES ('Rhin', 'Divers'); \
INSERT INTO Articles VALUES ('Romarin', 'Divers'); \
INSERT INTO Articles VALUES ('Horlogerie', 'Divers'); \
INSERT INTO Articles VALUES ('Temps', 'Divers'); \
INSERT INTO Articles VALUES ('Vitesse', 'Divers'); \
INSERT INTO Articles VALUES ('Vecteur', 'Divers'); \
INSERT INTO Articles VALUES ('Force', 'Divers'); \
INSERT INTO Articles VALUES ('Symbole', 'Divers'); \
INSERT INTO Articles VALUES ('Poterie', 'Divers'); \
INSERT INTO Articles VALUES ('Porcelaine', 'Divers'); \
INSERT INTO Articles VALUES ('Atome', 'Divers'); \
INSERT INTO Articles VALUES ('Sable', 'Divers'); \
INSERT INTO Articles VALUES ('Plage', 'Divers'); \
INSERT INTO Articles VALUES ('Parc', 'Divers'); \
INSERT INTO Articles VALUES ('Ville', 'Divers'); \
INSERT INTO Articles VALUES ('Capitale', 'Divers'); \
INSERT INTO Articles VALUES ('Latin', 'Divers'); \
INSERT INTO Articles VALUES ('Gaulois (peuples)', 'Divers'); \
INSERT INTO Articles VALUES ('Suffixe', 'Divers'); \
INSERT INTO Articles VALUES ('Clignotant', 'Divers'); \
INSERT INTO Articles VALUES ('Infrarouge', 'Divers'); \
INSERT INTO Articles VALUES ('Prisme', 'Divers'); \
INSERT INTO Articles VALUES ('Cheminée', 'Divers'); \
INSERT INTO Articles VALUES ('Turbine', 'Divers'); \
INSERT INTO Articles VALUES ('Moulin', 'Divers'); \
INSERT INTO Articles VALUES ('Cacao', 'Divers'); \
INSERT INTO Articles VALUES ('Chocolat blanc', 'Divers'); \
INSERT INTO Articles VALUES ('Torréfaction', 'Divers'); \
INSERT INTO Articles VALUES ('Coma', 'Divers'); \
INSERT INTO Articles VALUES ('Aéroport', 'Divers'); \
INSERT INTO Articles VALUES ('Radar', 'Divers'); \
INSERT INTO Articles VALUES ('Météorologie', 'Divers'); \
INSERT INTO Articles VALUES ('Foudre', 'Divers'); \
INSERT INTO Articles VALUES ('Méthane', 'Divers'); \
INSERT INTO Articles VALUES ('Euro', 'Divers'); \
INSERT INTO Articles VALUES ('Union européenne', 'Divers'); \
INSERT INTO Articles VALUES ('France', 'Divers'); \
INSERT INTO Articles VALUES ('Pain au chocolat', 'Divers'); \
INSERT INTO Articles VALUES ('Baguette (pain)', 'Divers'); \
INSERT INTO Articles VALUES ('Rolex', 'Divers'); \
INSERT INTO Articles VALUES ('Air France', 'Divers'); \
INSERT INTO Articles VALUES ('Wikipédia', 'Divers'); \
INSERT INTO Articles VALUES ('Déforestation', 'Divers'); \
INSERT INTO Articles VALUES ('Amazon', 'Divers'); \
INSERT INTO Articles VALUES ('Apple', 'Divers'); \
INSERT INTO Articles VALUES ('Novembre', 'Divers'); \
INSERT INTO Articles VALUES ('Trisomie 21', 'Divers'); \
INSERT INTO Articles VALUES ('Recyclage', 'Divers'); \
INSERT INTO Articles VALUES ('Minecraft', 'Divers'); \
INSERT INTO Articles VALUES ('Lune', 'Divers'); \
INSERT INTO Articles VALUES ('Disneyland Paris', 'Divers'); \
INSERT INTO Articles VALUES ('Neige', 'Divers'); \
INSERT INTO Articles VALUES ('Avalanche', 'Divers'); \
INSERT INTO Articles VALUES ('Emmental', 'Divers'); \
INSERT INTO Articles VALUES ('Camembert (fromage)', 'Divers'); \
INSERT INTO Articles VALUES ('Crème fraîche', 'Divers'); \
INSERT INTO Articles VALUES ('Fer à repasser', 'Divers'); \
INSERT INTO Articles VALUES ('Afrique', 'Divers'); \
INSERT INTO Articles VALUES ('Amérique', 'Divers'); \
INSERT INTO Articles VALUES ('Canal de Suez', 'Divers'); \
INSERT INTO Articles VALUES ('Océan Atlantique', 'Divers'); \
INSERT INTO Articles VALUES ('Terre', 'Divers'); \
INSERT INTO Articles VALUES ('Jeux olympiques', 'Divers'); \
INSERT INTO Articles VALUES ('Piscine', 'Divers'); \
INSERT INTO Articles VALUES ('Statistique', 'Divers'); \
INSERT INTO Articles VALUES ('La Joconde', 'Divers'); \
INSERT INTO Articles VALUES ('Musée du Louvre', 'Divers'); \
INSERT INTO Articles VALUES ('Viande', 'Divers'); \
INSERT INTO Articles VALUES ('Escalade', 'Divers'); \
INSERT INTO Articles VALUES ('Québec', 'Divers'); \
INSERT INTO Articles VALUES ('Oreille', 'Divers'); \
INSERT INTO Articles VALUES ('Foin', 'Divers'); \
INSERT INTO Articles VALUES ('Piano', 'Divers'); \
INSERT INTO Articles VALUES ('Tintin', 'Divers'); \
INSERT INTO Articles VALUES ('Astérix', 'Divers'); \
INSERT INTO Articles VALUES ('Star Wars', 'Divers'); \
INSERT INTO Articles VALUES ('Jurassic Park', 'Divers'); \
INSERT INTO Articles VALUES ('Cendrillon', 'Divers'); \
INSERT INTO Articles VALUES ('Les Dents de la mer', 'Divers'); \
INSERT INTO Articles VALUES ('Toy Story', 'Divers'); \
INSERT INTO Articles VALUES ('Pulp Fiction', 'Divers'); \
INSERT INTO Articles VALUES ('Gratte-ciel', 'Divers'); \
INSERT INTO Articles VALUES ('Neurone', 'Divers'); \
INSERT INTO Articles VALUES ('Saxophone', 'Divers'); \
INSERT INTO Articles VALUES ('Bugatti', 'Divers'); \
INSERT INTO Articles VALUES ('La Marseillaise', 'Divers'); \
INSERT INTO Articles VALUES ('Vendredi', 'Divers'); \
INSERT INTO Articles VALUES ('Cloche', 'Divers'); \
INSERT INTO Articles VALUES ('Clown', 'Divers'); \
INSERT INTO Articles VALUES ('République', 'Divers'); \
INSERT INTO Articles VALUES ('Camping', 'Divers'); \
INSERT INTO Articles VALUES ('Bowling', 'Divers')")