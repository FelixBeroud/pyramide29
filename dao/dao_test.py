import sys
sys.path.append('../pyramide29')
"""
Ce fichier est un test pour la DAO, utilisant les classes dev.

Il construit une partie (constituée de 5 manches) puis tente de les mettre dans la bdd.
"""


from article import Article
from reglage import Reglage
from manche import Manche
from partie import Partie
from login import Login
from joueuse import Joueuse

from requetedao import RequeteDAO
from joueusedao import JoueuseDAO

try: 
    felix = RequeteDAO().obtenir_joueuse("Felix")
except ValueError:
    felix = Joueuse(login = Login(nom = "Felix", mdp = "super_mdp_secret"))
    felix.login.chiffrer_mdp()
    JoueuseDAO().creer_joueuse(felix)


manche1 = Manche(Article(titre = "Canada"), nombre_tentatives = 4, score_manche = 6, liste_indices_tot= ["placeholder", "placeholder"])
manche2 = Manche(Article(titre = "Mulholland Drive"), nombre_tentatives = 1, score_manche = 10.5, liste_indices_tot= ["placeholder", "placeholder"])
manche3 = Manche(Article(titre = "Jake Cooper"), nombre_tentatives = 8, score_manche = 0, liste_indices_tot= ["placeholder", "placeholder"])
manche4 = Manche(Article(titre = "Natalie Wynn"), nombre_tentatives = 2, score_manche = 9, liste_indices_tot= ["placeholder", "placeholder"])
manche5 = Manche(Article(titre = "Good morning England"), nombre_tentatives = 4, score_manche = 6, liste_indices_tot= ["placeholder", "placeholder"])

partie_test = Partie(felix, score_partie = 31.5, liste_manches=[manche1, manche2, manche3, manche4, manche5], reglage = Reglage("Normal", 3, 5, 1.5))

from interfacedao import InterfaceDAO   

InterfaceDAO().updater_bdd(game = partie_test)
print("Score accumulé après update : ", felix.score_acc)
