import sys
sys.path.append('../pyramide29')

from login import Login
from joueuse import Joueuse
from dao.connection import DBConnection

class JoueuseDAO:
    """
    Classe interfaçant la classe métier Joueuse avec la BDD.
    Les attributs ne sont pas tout à fait les mêmes d'un côté et de l'autre.
    """
    def creer_joueuse(self, joueuse):
        coco = DBConnection().connection
        with coco:
            with coco.cursor() as cursor:
                # 1. Vérifier qu'il n'y a pas déjà une personne qui s'appelle comme ça
                cursor.execute(
                    "SELECT COUNT(pseudo) FROM Joueuse WHERE pseudo = %(pseudo)s "
                , {
                    "pseudo" : joueuse.login.nom
                })
                q = cursor.fetchall()[0]['count']
                # récupère le nombre d'éléments ('count') renvoyé par la requête sql

                if q == 1:
                    raise ValueError('Création impossible. Il existe déjà une personne portant ce nom dans la BDD. Veuillez choisir un autre nom.')
                
                if q > 1:
                    raise ValueError('Erreur grave : il existe déjà PLUSIEURS personnes portant ce nom dans la BDD, c\'est pas normal et il faut faire quelque chose là')
                
                else:
                # 2. Remplir les champs en ligne
                    cursor.execute(
                        "INSERT INTO Joueuse(pseudo, mot_de_passe, date_inscription, score_total, score_moyen, nombre_parties)" \
                            "VALUES (%(pseudo)s, %(mot_de_passe)s, %(date_inscription)s, %(score_total)s, %(score_moyen)s, %(nombre_parties)s)",
                            {
                                "pseudo" : joueuse.login.nom,
                                "mot_de_passe" : joueuse.login.mdp,
                                "date_inscription" : joueuse.date_inscription,
                                "score_total" : joueuse.score_acc,
                                "score_moyen" : joueuse.score_moy,
                                "nombre_parties" : int(joueuse.score_acc/joueuse.score_moy if joueuse.score_moy != 0 else 0) # à arranger...
                            }
                    )

if __name__== "__main__":
    bobz = Login("Mx. Test", "mdp_secret_chiffré")
    bobz.chiffrer_mdp()
    mx_test = Joueuse(bobz)
    JoueuseDAO().creer_joueuse(mx_test) # devrait faire apparaître une nouvelle Mx Test dans la bdd Joueuse