import sys
sys.path.append('../pyramide29')

from dao.connection import DBConnection
from joueuse import Joueuse
from login import Login
from article import Article


class RequeteDAO:
    """
    Classe regroupant la plupart des requêtes qu'on envoie à la bdd, et qui renvoie des objets métier.
    Elle contient : 
    - obtenir_joueuse(nom) : À partir d'un nom (chaîne de caractères), crée et renvoie l'objet métier Joueuse correspondant.
    - checker_mdp(login) : Vérifie si un login entré (pseudo + mdp chiffré) correspond à ce qui est stocké sur la bdd, pour s'authentifier.
    - obtenir_page_joueuse(nom) : renvoie **dans la console** quelques informations sur la joueuse dont on a entré le nom.
    - obtenir_highscores : renvoie **dans la console** le top 10 des meilleurs joueur.euses.
    - obtenir_articles_categorie(categorie, n) : Renvoie une liste de n articles issus de la catégorie désignée
    """

    def obtenir_joueuse(self, nom) -> Joueuse:
        """
        À partir d'un nom (chaîne de caractères), crée et renvoie l'objet métier Joueuse correspondant.

         Attributes
        ----------
        nom (str): nom du profil à chercher sur la bdd.
        """
        coco = DBConnection().connection
        with coco:
            with coco.cursor() as cursor:
                cursor.execute(
                    "SELECT * FROM Joueuse WHERE pseudo = %(nom)s ",
                    {
                        "nom" : nom
                    }
                )
                res = cursor.fetchall()
                if len(res)>1:
                    raise ValueError('Erreur grave : il y a plusieurs entrées avec ce nom, et ce n\'est pas normal')
                
                if len(res)==0:
                    raise ValueError('Erreur : aucune personne de la base ne correspond à ce nom')

                j_bdd = res[0]
                renvoi = Joueuse(
                    login = Login(nom = j_bdd['pseudo'], mdp = j_bdd['mot_de_passe']),
                    date_inscription = j_bdd['date_inscription'],
                    score_acc= float(j_bdd['score_total']),
                    score_moy= float(j_bdd['score_moyen'])
                )
                renvoi.login.mdp = j_bdd['mot_de_passe']
                return renvoi
                
    def checker_mdp(self, login_a_checker:Login):
        """Entre un login (nom et mdp) et vérifie :
        1. s'il existe quelqu'un avec ce nom dans la bdd
        2. si le mdp est le bon
        (à cette étape, les deux mdp doivent déjà être chiffrés)

        Cette fonction renvoie 1 si le login est correct, 0 sinon.
        Si aucune joueuse de la BDD n'a le nom voulu, une erreur est renvoyée.

         Attributes
        ----------
        login_a_checker (Login): login (dont le mdp a été chiffré) qu'on veut comparer à celui stocké sur la bdd pour s'authentifier.
        """

        joueuse_correspondante = self.obtenir_joueuse(login_a_checker.nom)
        if joueuse_correspondante.login.nom == "AUCUNE":
            raise ValueError("Erreur : aucune joueuse n'a ce nom dans la BDD")
        else:
            if joueuse_correspondante.login.mdp == login_a_checker.mdp:
                return 1
            else:
                return 0

    def obtenir_page_joueuse(self, nom):
        """
        Cette méthode renvoie **dans la console** quelques informations sur la joueuse dont on a entré le nom.
        Utilisée pour consulter la bdd.

        Pour obtenir l'objet-métier Joueuse correspondant à un nom, utiliser obtenir_joueuse.
         Attributes
        ----------
        nom (str): nom du profil à chercher sur la bdd.
        """

        joueuse = self.obtenir_joueuse(nom)
        print(
            f"PSEUDO : {joueuse.login.nom}" \
            f"\nScore moyen : {joueuse.score_moy}"\
            f"\nScore accumulé : {joueuse.score_acc}"
        )
        if joueuse.score_moy != 0:
            print(f"\nNombre de parties jouées : {int(joueuse.score_acc/joueuse.score_moy)}")
    
    def obtenir_highscores(self, n = 10):
        """
        Affiche les n meilleurs joueur/euses dans la console.
        "Meilleur" est défini comme "ayant le meilleur score moyen".

         Attributes
        ----------
        n (int): nombre de profils à retirer.
        """
        coco = DBConnection().connection
        with coco:
            with coco.cursor() as cursor:
                cursor.execute(
                    "SELECT pseudo, score_moyen, nombre_parties FROM Joueuse ORDER BY score_moyen DESC"
                )
                top = cursor.fetchmany(n)
        if len(top)==0:
            print("Il n'y a personne dans la BDD. Il faudrait penser à jouer un peu avant de regarder les highscores...")
        else:
            for num in range(n):
                if num >= len(top): 
                    break
                print(
                    f"N°{num+1} : {top[num]['pseudo']}, score moyen : {round(top[num]['score_moyen'],1)}, sur {top[num]['nombre_parties']} parties"
                    )

    def obtenir_articles_categories(self, categorie:str, n=5):
        """
        Renvoie une liste de n=5 articles issus de la catégorie désignée.
        Pensé pour remplacer GetArticles().obtenir_article lorsque l'on veut jouer avec une catégorie donnée.

         Attributes
        ----------
        categorie (str):
            nom de la catégorie de laquelle on veut tirer des articles. Doit être parmis Pays, Villes, Animaux, Personnes célèbres, Divers, ou Aléatoire.
        n (int):
            nombre d'articles à tirer.
        """
        if categorie != "Aléatoire":
            coco = DBConnection().connection
            with coco:
                with coco.cursor() as cursor:
                    cursor.execute(
                        "SELECT titre_complet FROM Articles WHERE categorie = %(cat)s ORDER BY random()"
                    , {"cat":categorie})
                    res = cursor.fetchall()
                    liste_renvoi = []
                    for num in range(n):
                        liste_renvoi.append(Article(res[num]['titre_complet']))
                    return liste_renvoi
        
        if categorie == "Aléatoire":
            coco = DBConnection().connection
            with coco:
                with coco.cursor() as cursor:
                    cursor.execute(
                        "SELECT titre_complet FROM Articles ORDER BY random()"
                    , {"cat":categorie})
                    res = cursor.fetchall()
                    liste_renvoi = []
                    for num in range(n):
                        liste_renvoi.append(Article(res[num]['titre_complet']))
                    return liste_renvoi
            

if __name__ == "__main__":
    #RequeteDAO().obtenir_page_joueuse("Felix")
    #RequeteDAO().obtenir_highscores()
    cinqtrucs = RequeteDAO().obtenir_articles_categories("Aléatoire")
    print(cinqtrucs[0].titre)
