"""
Ce script, ainsi que celui de init_categories.py, ne sont à exécuter SEULEMENT si les tables n'ont pas été créées avec le fichier init_db.sql fourni.
Ce script est à exécuter une seule fois. Il crée sur Postgre les tables Joueuse, Partie et Manche.
Attention, son exécution efface les éventuelles tables qui existent déjà, donc à n'utiliser qu'avec précaution.
"""

from connection import DBConnection
coco = DBConnection().connection

if input('Êtes-vous sûr de vouloir réinitialiser les tables Postgre ? Cette opération est irréversible. Entrez OUI pour réinitialiser les tables : ') == 'OUI':
    with coco:
        with coco.cursor() as cursor:
            cursor.execute(
                "DROP TABLE IF EXISTS Joueuse, Partie, Manche ; \
                CREATE TABLE Joueuse (id_joueuse SERIAL UNIQUE PRIMARY KEY, \
                    pseudo VARCHAR NOT NULL,\
                    mot_de_passe VARCHAR NOT NULL,\
                    date_inscription DATE, \
                    score_total NUMERIC (10, 6), \
                    score_moyen NUMERIC (10, 6), \
                    nombre_parties INTEGER\
                    ); \
                CREATE TABLE Partie (id_partie SERIAL UNIQUE PRIMARY KEY, \
                    id_joueuse INTEGER NOT NULL,\
                    date_partie DATE,\
                    score_partie NUMERIC (10, 6), \
                    difficulte VARCHAR\
                    ); \
                CREATE TABLE Manche (id_manche SERIAL UNIQUE PRIMARY KEY, \
                    id_partie INTEGER NOT NULL,\
                    score_manche NUMERIC (10, 6), \
                    titre VARCHAR\
                    ); \
                        "
                )
            cursor.execute(
                "INSERT INTO Joueuse(pseudo, mot_de_passe, date_inscription, score_total, score_moyen, nombre_parties) \
                    VALUES ('anonyme', 'anonyme', '1970-01-01', 0, 0, 0)"
            )

# Pour faire des premiers tests, cf. main_test.py