from login import Login
from datetime import datetime

class Joueuse():
    """Classe permettant d'implementer une joueuse avec son login, sa date d'inscription, son score accumulé et son score moyen
    
    Attributes
    ----------
    login (Login):
        le login de la joueuse
    date_inscription (date):
        date de la creation du profil joueuse
    score_acc (float):
        score accumulé de la joueuse au fil des parties
    score_moy (float):
        score moyen de la joueuse pour ses parties
    """

    def __init__(self, login, date_inscription = datetime.now(), score_acc = 0, score_moy = 0):
        """Initialisation de la classe"""

        self.login = login
        self.date_inscription = date_inscription
        self.score_acc = score_acc
        self.score_moy = score_moy

    




