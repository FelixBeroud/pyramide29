from calculeIndice import CalculeIndice
from reglage import Reglage
from joueuse import Joueuse
from getArticle import GetArticle
from manche import Manche
from article import Article
from InquirerPy import inquirer
from dao.requetedao import RequeteDAO

class Partie():
    """Classe permettant d'implémenter une partie qui sera caractérisée par des réglages, un utilisateur et finalement un score.
    
    Attributes
    ---------
    joueuse (Joueuse):
        joueuse qui va jouer la partie
    reglage (Reglage):
        reglage de la partie
    score_partie (int):
        score de la partie qui est initialisé à 0
    liste_manches (list[Manche]):
        liste des manches que contient la partie
    """

    def __init__(self, joueuse, reglage, score_partie=0, liste_manches=[]):
        """Initialisation de la classe partie"""
        self.score_partie = score_partie
        self.joueuse = joueuse
        self.reglage = reglage
        self.liste_manches = liste_manches

    def start_partie(self):
        """Méthode permettant de commencer une partie
        
        Returns
        -------
        bool : Si True, les scores doivent être sauvegardés, sinon ils ne le seront pas (choix de l'utilisateur).
        """

        print("""
        ***
        Début de la partie
        ***
        """)

        nombre_manches = 5

        articles_test = ["Bruz", "Gouttière", "Marne (département)", "Buzz l'Éclair", "Stylo"] # Set de test.


        # Détermination des articles de la partie
        if self.reglage.difficulte == "Jouer sur un set de test":
            articles = [Article(titre = articles_test[i]) for i in range(5)]
        elif self.reglage.categorie == "Tout Wikipédia":
            getarticles = GetArticle()
            articles = getarticles.obtenir_article(nombre_manches)
        else:
            req = RequeteDAO()
            articles = req.obtenir_articles_categories(self.reglage.categorie) #La recherche d'articles issus des catégories s'effectue par recherche dans la base de données via la DAO

        # Détermination des indices pour chaque article
        indices = []
        for art in articles:
            cherche_indices = CalculeIndice()
            tous_les_indices = cherche_indices.extraire_indices(article = art)
            indices.append(cherche_indices.filtrage(liste_indices = tous_les_indices, nom_article = art.titre, n = self.reglage.nombre_indices_init + self.reglage.nombre_tentatives_max - 1))
        
        liste_manches = []

        # Tour de boucle effectué à chaque manche
        for i in range(5):

            #Initialisation et lancement de la manche
            manche_en_cours = Manche(articles[i], nombre_tentatives = 0, score_manche = 0, liste_indices_tot = indices[i], reglage = self.reglage)
            liste_manches.append(manche_en_cours)
            manche_en_cours.start_manche()

            #Mise à jour du score
            self.score_partie += manche_en_cours.score_manche

            #Proposition de continuer vers la manche suivante si l'on n'a pas atteint la dernière manche.
            if i<4:
                action = inquirer.select(
                    message="Nouvelle manche ?",
                    choices=[
                        "Oui !",
                        "Non",
                    ],
                    default=None,
                ).execute()
            
                if action == "Non":
                    break

        print("\nScore final de la partie : " + str(self.score_partie) + " points.\n")
        sauvegarde = inquirer.select(
                        message="Souhaitez-vous sauvegarder votre score et votre historique ?",
                        choices=["Oui !", "Non"],
                        default=None).execute()

        return sauvegarde 