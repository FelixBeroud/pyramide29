from article import Article
import random
import requests
from difflib import SequenceMatcher
import unittest

class CalculeIndice():
    """Classe implémentant le calcul des indices pour un article donné. Il n'a pas d'attributs."""

    def extraire_indices(self, article):
        """Méthode renvoyant une liste d'indices pour un article donné

        Les indices sont les liens hypertextes de la page (titres des pages d'article vers lesquels ils redirigent).
        
        Arguments
        ---------
        article : Article
            article pour lequel on cherche les indices
            
        Returns
        -------
        list : la liste de tous les indices possibles (éventuellement filtrés ensuite)
        """

        # Requête 
        URL_API = "https://fr.wikipedia.org/w/api.php"
        PARAM = {
            "action" : "query",
            "prop": "links",
            "titles" : article.titre,
            "pllimit" : 500,   # limite maximale de 500 liens hypertextes sur une page.
            "plnamespace" : 0, # pour retirer tous les liens de type "Portail:" et autres préfixes
            "format" : "json"}
        res = requests.get(url=URL_API, params=PARAM)
        res=res.json()

        # Mise en forme des résultats dans une liste de liens qui serviront de base pour les indices
        idpage = list(res["query"]["pages"].keys())[0]

        indices = []

        infos = res["query"]["pages"][idpage]["links"] # Permet de récupérer les liens de la page
        for info in infos:
            indices.append(info["title"]) # Récupère uniquement le titre de la page comme indice

        indices = list(set(indices)) # Permet de rendre les indices uniques.
        
        return indices
    
    def filtrage(self, liste_indices, nom_article, n):
        """Méthode permettant de filtrer les indices quand ils ne sont pas convenables.

        Indices non valides :
        - Articles avec un mot trop proche de l'article à trouver
        - Articles de mois de l'année
        - Articles correspondant à des dates ou des années
        - Articles avec des mots de 2 caractères ou moins
        
        Arguments
        ---------
        liste_indices : list
            liste d'indices à filtrer
        nom_article : str
            titre de l'article dont les indices sont à filtrer
        n : int
            nombre d'indices maximal à renvoyer
            
        Returns
        -------
        list : la liste des indices valides après filtrage
        """

        indices_filtres = []

        for indice in liste_indices:
            conditions_remplies = True
            
            # Séparation des titres d'articles (indices) en une liste de mots qui les composent
            if " " in indice:
                espaces = [-1] # Ajoute la position du début de la chaîne moins un.

                for (index, caractere) in enumerate(indice):
                    if caractere == " ":
                        espaces.append(index) # Stockage des positions des espaces qui séparent les mots

                espaces.append(len(indice)) # On rajoute la dernière lettre du nom de la page pour pouvoir permettre le découpage

                indice_decoupe = [indice[espaces[i]+1:espaces[i+1]] for i in range(len(espaces)-1)] # création de la liste contenant les différents mots composant les indices
            else:
                indice_decoupe = [indice] # Si l'indice est un seul mot, alors la liste est simplement l'indice
            
            for mot in indice_decoupe:
                if SequenceMatcher(None, mot, nom_article).ratio() > 0.4: # si la proximité de l'indice avec le titre de l'article à trouver est trop forte
                    conditions_remplies = False
                elif mot in ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"]:
                    conditions_remplies = False
                elif mot.isdigit() and len(mot) > 3: #repère les dates (années)
                    conditions_remplies = False
                elif len(mot) < 3:
                    conditions_remplies = False
            
            if conditions_remplies:
                indices_filtres.append(indice) #filtrage des indices : un indice est ajouté seulement si les conditions sont remplies

        if len(indices_filtres) > n: # si plus d'indices sont éligibles que nécessaire, on en tire n au sort
            indices = random.choices(indices_filtres,k=n)
        else:
            indices = indices_filtres
        
        return indices


class TestsIndices(unittest.TestCase):

    def test_tri_mots_proches(self):
        """Réalise le test suivant :
        Les indices trop proches du nom de l'article sont filtrés, les autres sont conservés."""
        C = CalculeIndice()
        article = Article("Gâche de Guernesey")
        indices = C.extraire_indices(article)
        self.assertIn("Guernesey (île)", indices)
        self.assertIn("Gâche", indices)
        self.assertIn("Cuisine normande", indices)
        
        indices_valides = C.filtrage(indices, article.titre, 30)
        self.assertNotIn("Guernesey (île)", indices_valides)
        self.assertNotIn("Gâche", indices_valides)
        self.assertIn("Cuisine normande", indices_valides)

    def test_tri_dates(self):
        """Réalise le test suivant :
        Quand l'article contient des dates (années, mois...), elles sont supprimées des indices. Les autres indices sont conservés."""
        C = CalculeIndice()
        article = Article("Krach de 1929")
        indices = C.extraire_indices(article)
        self.assertIn("Octobre 1929", indices)
        self.assertIn("1929", indices)
        self.assertIn("Années 1920", indices)
        self.assertIn("Wall Street", indices)

        indices_valides = C.filtrage(indices, article.titre, 1000)
        self.assertNotIn("Octobre 1929", indices_valides)
        self.assertNotIn("1929", indices_valides)
        self.assertNotIn("Années 1920", indices_valides)
        self.assertIn("Wall Street", indices_valides)

    def test_nombre_indices_insuffisant(self):
        """Réalise le test suivant :
        Quand le nombre d'indices est insuffisant, seuls les indices valides sont renvoyés même s'il n'y en a pas assez."""
        C = CalculeIndice()
        article = Article("Gâche de Guernesey")
        indices = C.extraire_indices(article)
        indices_valides = C.filtrage(indices, article.titre, 20)
        self.assertFalse(len(indices_valides) == 20)

    def test_limite_liens(self):
        """Réalise le test suivant :
        Quand l'article contient plus de 500 liens hypertextes, seuls 500 sont retenus à cause de la limite de l'API."""
        C = CalculeIndice()
        article = Article("Paris")
        indices = C.extraire_indices(article)
        self.assertEqual(len(indices), 500)

    def test_respect_nombre_indices(self):
        """Réalise le test suivant :
        Quand le nombre d'indices filtrés est suffisant, le nombre d'indices souhaité est bien renvoyé."""
        C = CalculeIndice()
        article = Article("Paris")
        indices = C.extraire_indices(article)
        indices_valides = C.filtrage(indices, article.titre, 20)
        self.assertEqual(len(indices_valides), 20)

if __name__ == '__main__':
    unittest.main()