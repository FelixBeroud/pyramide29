from InquirerPy import inquirer
from unidecode import unidecode

class Manche:
    """Classe implémentant une manche dans une partie
    Attributes
    ----------
    article:Article
        article sélectionné
    nombre_tentatives:int
        nombre de tentatives déjà utilisées
    liste_indices: list
        liste des indices pour l'article sélectionné
    score_manche (int):
        score de la manche
    reglage: reglage
        réglages de la manche (qui sont normalement ceux de la partie)
    """

    def __init__ (self,article, nombre_tentatives, liste_indices_tot, score_manche = 0, reglage=None):
        """Initialisation de la manche"""
        self.article = article
        self.nombre_tentatives = nombre_tentatives
        self.liste_indices_tot = liste_indices_tot
        self.score_manche = score_manche
        self.reglage = reglage
        

    def donner_indices(self):
        """Méthode fournissant les indices au joueur, ne renvoie rien."""
        if self.nombre_tentatives==0 : # = si c'est le début de la manche
            if len(self.liste_indices_tot) > self.reglage.nombre_indices_init: # Si le nombre d'indices est suffisant
                print("Voici les {} premiers indices :".format(self.reglage.nombre_indices_init))
                print(" | ".join(self.liste_indices_tot[:self.reglage.nombre_indices_init]))
            else: # Si le nombre d'indices est insuffisant
                print("Voici les seuls indices disponibles :")
                print(" | ".join(self.liste_indices_tot))
        elif len(self.liste_indices_tot) < self.nombre_tentatives+self.reglage.nombre_indices_init: # Si la liste d'indices disponibles est épuisée
            print("Aucun nouvel indice disponible.")
        else:
            print("Le nouvel indice est : {}".format(self.liste_indices_tot[self.nombre_tentatives+self.reglage.nombre_indices_init-1]))

    def proposer_tentative(self):
        """Méthode implémentant la proposition de tentative et la réponse associée
        
        Returns
        -------
        bool
            True si le nom de l'article a été trouvé, False sinon."""

        essai = inquirer.text(message = "Quel est le nom de l'article:").execute()
        self.nombre_tentatives+=1
        if unidecode(essai).upper() == unidecode(self.article.titre_court).upper(): #gère les approximations majuscule/minuscule, ignore les précisions entre parenthèses et les accents
            
            self.score_manche=((8-self.nombre_tentatives)*self.reglage.facteur_multip)
            print("Bravo vous avez trouvé ! Cette manche vous a rapporté " + str(self.score_manche) + " points.\n")
            return True
        else:
            print("Désolé, il vous reste {} tentatives".format(self.reglage.nombre_tentatives_max-self.nombre_tentatives))
            return False


    def start_manche(self):
        """Méthode permettant de commencer une manche, ne renvoie rien."""

        print("\n*** NOUVELLE MANCHE *** \n")
        
        self.donner_indices()

        # Boucles réalisées jusqu'à ce que le joueur trouve ou que le nombre de tentatives maximal est atteint.
        while (self.nombre_tentatives < self.reglage.nombre_tentatives_max):
            trouve = self.proposer_tentative()
            if trouve == True:
                break
            elif self.nombre_tentatives < self.reglage.nombre_tentatives_max:
                self.donner_indices()
            else:
                print("Perdu : l'article était "+ self.article.titre)