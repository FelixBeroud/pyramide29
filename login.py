import hashlib

class Login():
    """Classe permettant d'implementer un processus de connexion avec un nom et un mot de passe 
    
    Attributes
    ----------
    nom (str):
        le nom de l'utilisateur
    mdp (str):
        le mot de passe de l'utilisateur    
    """

    def __init__(self, nom, mdp=None):
        """Initialistion de la classe"""
        self.nom = nom
        self.__mdp = mdp
            
    def chiffrer_mdp(self):
        """Méthode permettant de chiffrer tous les mots de passe. Ne renvoie rien."""
        salt = self.nom
        self.mdp = hashlib.sha256(salt.encode() + self.__mdp.encode()).hexdigest()