from article import Article
import wikipedia
import requests

class GetArticle:
    """Classe implémentant la recherche d'articles Wikipedia."""
    def separer_par_page(self, dictionnaire):
        """Méthode utilitaire pour les sorties de l'API de Wikipedia.
        
        Permet de séparer la sortie de l'API (dictionnaire) en une liste de dictionnaires, un dictionnaire par page Wikipedia
        
        Attributes
        ----------
        dictionnaire : dict
            Dictionnaire à séparer par page
            
        Returns
        -------
        list : Liste de dictionnaires, chaque dictionnaire représentant une page Wikipedia
        """

        listepages = []
        for cle, value in dictionnaire['query']['pages'].items():
            listepages.append({cle : value})
        return listepages

    def aplatir(self, dictionnaire):
        """Méthode récursive qui renvoie un dictionnaire avec un seul niveau hiérarchique
    
        Les clés du dictionnaire renvoyé ne contiennent aucun dictionnaire comme valeur correspondante, seulement un float, int, str, bool ou None.

        Parameters
        ----------
        dictionnaire : dict
            dictionnaire que l'on souhaite "aplatir"

        Returns
        -------
        dict
            dictionnaire à un seul niveau hiérarchique : les clés ne peuvent pas avoir comme valeur correspondante un (sous-)dictionnaire
        """
        items = []
        i=0
        for cle, valeur in dictionnaire.items():
            i+=1
            if isinstance(valeur, dict):
                items.extend(self.aplatir(valeur).items())
            else:
                cle = str(i)
                items.append((cle, valeur))
        return dict(items)

    
    def moyenne_dictionnaire(self, dictionnaire):
        """Méthode réalisant la moyenne des valeurs d'un dictionnaire
        
        Attributes
        ----------
        dictionnaire : dict
            Dictionnaire sur lequel on veut faire la moyenne
            
        Returns
        -------
        float : Moyenne des valeurs du dictionnaire
        """
        count = 0
        sum = 0
        for value in dictionnaire.values():
            if value==None:
                value=0
            count += 1
            sum += value
        return sum/count

    def obtenir_article(self, n):
        """Méthode tirant n articles correspondant aux valeurs de seuil définies
        
        Attributes
        ----------
        n : int
            Nombre d'articles à tirer
            
        Returns
        -------
        list(Article) : liste d'articles qui seront utilisés dans le jeu
        """
        
        print("Recherche d'articles...\nLe temps de chargement de ce mode peut être long :\nLa recherche d'articles se fait dans l'ensemble de Wikipedia.\n")
        fenetre_jours = 30 #nombre de jours sur lequel on va calculer la moyenne de vues quotidiennes
        liste_articles = []
        wikipedia.set_lang("fr")
        URL_API = "https://fr.wikipedia.org/w/api.php"
        
        while len(liste_articles) < n : # tant que n articles valides n'ont pas été tirés
            echantillon = wikipedia.random(5) #tirage de 5 articles au hasard dans tout Wikipedia français avec le package Wikipedia. La limite de 5 est fixée par le fait que si on en met davantage, la requête API est divisée en 2 parties, qu'il est coûteux de ré-assembler.
            
            # Requête à l'API pour obtenir le nombre de vues quotidiennes sur X jours précédents (X=fenetre_jours)
            echantillon_str = "|".join(echantillon)
            PARAM = {
                "action" : "query",
                "prop": "pageviews",
                "titles" : echantillon_str,
                "pvipdays" : fenetre_jours,
                "format" : "json"}
            res = requests.get(url=URL_API, params=PARAM)
            res=res.json()

            #Traitement des données récupérées
            res = self.separer_par_page(res) #Division du dictionnaire contenant les données des 5 pages en un dictionnaire par page
            for k in range(len(res)):
                res[k] = self.aplatir(res[k]) # Elimination des sous-dictionnaires pour un résultat plus facilement lisible et analysable
                if len(res[k].keys()) == fenetre_jours: # Repérage de l'item de chaque dictionnaire qui correspond aux vues quotidiennes sur 30 jours
                    moy=self.moyenne_dictionnaire(res[k]) # Moyenne des vues quotidiennes
                    if moy > 600: #seuil (arbitraire, mais en-dessous de ce seuil, il n'y a quasiment que des pages introuvables.)
                        print("Article " + str(len(liste_articles) + 1) +" trouvé.") # Message pour tenir au courant l'utilisateur que les articles sont trouvés petit à petit
                        liste_articles.append(Article(echantillon[k]))
        
        return liste_articles[:n] # Au cas où dans une boucle, plusieurs articles valides sont trouvés et donc la longueur de liste_articles dépasserait n
